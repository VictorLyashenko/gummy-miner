'use strict';

var startButton = document.querySelector('#startButton');
var gummy = document.querySelector('#gummy');
var infoText = document.querySelector("#info");
var timeText = document.querySelector("#time");
var resultInfo = document.querySelector('#resultInfo');
var count = document.querySelector('#count');
var counter = 0;
var selectLevel = document.querySelector('#select');
var candiesUrl = ['img/candy.svg', 'img/gummy-bear.svg','img/lollipop_1.svg','img/lollipop_2.svg','img/marshmallow.svg','img/sweet_1.svg','img/sweet_2.svg'];
var randomSrite;

function gummyRandom () {
	randomSrite = Math.floor(Math.random() * (candiesUrl.length));
	gummy.src = candiesUrl[randomSrite]

	var res = resultInfo.children[randomSrite]
	counter = res.children[1]
}

var candy = document.querySelector('#candy');

var gummiesCounter = 0;

var setGummyPosition = function (){
	gummy.style.left = Math.round(Math.random() * 90) + '%';
	gummy.style.top = Math.round(Math.random() * 90) + '%';
	gummyRandom ()
};

startButton.addEventListener('click', function (event){

	event.preventDefault();

	gummy.style.display = 'block';
	setGummyPosition();
		
	startButton.setAttribute('disabled', 'disabled');
	infoText.textContent = "You score 0";
	var duration = 10;
	timeText.textContent = duration + " seconds left";

	function counterGummies () {

		setGummyPosition();
		clearInterval(gummyInterval);
		gummyInterval = setInterval(setGummyPosition, 1000);
		counter.textContent++
		gummiesCounter = gummiesCounter + randomSrite + 1;
		infoText.textContent = "You score " + gummiesCounter;
	}

	gummy.addEventListener('click', counterGummies);

	setTimeout(function() {
		infoText.textContent = "You result is " + gummiesCounter + " !";
		startButton.removeAttribute('disabled');
		gummy.removeEventListener('click', counterGummies);
	},10000);

	var gummyInterval = setInterval(setGummyPosition, Number(selectLevel.value));

	var durationInterval = setInterval(function (){
		duration--;
		timeText.textContent = duration + " seconds left";

		if(duration === 0){
			timeText.textContent = "Time out!!!";
			clearInterval(durationInterval);
			clearInterval(gummyInterval);
			gummiesCounter = 0;
			gummy.style.display = 'none';
		}
	}, 1000)
});
